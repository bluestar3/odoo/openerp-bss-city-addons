# -*- coding: utf-8 -*-
# Part of Partners Postal Codes Search.
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    postal_code_id = fields.Many2one('bss.postal_code', "City Search")

    @api.onchange('postal_code_id')
    def _onchange_postal_code_id(self):
        if self.postal_code_id:
            self.country_id = self.postal_code_id.country_id
            self.state_id = self.postal_code_id.state_id
            self.zip = self.postal_code_id.postal_code
            self.city = self.postal_code_id.long_name
            self.postal_code_id = None

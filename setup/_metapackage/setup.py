import setuptools

with open('VERSION.txt', 'r') as f:
    version = f.read().strip()

setuptools.setup(
    name="odoo10-addons-odoo10-addons-bss-city",
    description="Meta package for odoo10-addons-bss-city Odoo addons",
    version=version,
    install_requires=[
        'odoo10-addon-bss_city',
        'odoo10-addon-bss_city_ch',
        'odoo10-addon-bss_city_fr',
        'odoo10-addon-bss_crm_city_search',
        'odoo10-addon-bss_partner_city_search',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Odoo',
    ]
)

# -*- coding: utf-8 -*-
# Part of Postal Codes.
# See LICENSE file for full copyright and licensing details.

import base64
import csv
import io
from odoo import api, fields, models
from odoo.netsvc import logging
from odoo.osv import osv
from odoo.tools.translate import _


class bssImportStates(models.TransientModel):
    _name = 'bss.import_states'
    _description = 'Import States'
    _logger = logging.getLogger(_name)

    country_id = fields.Many2one(
        'res.country', "Country", required=True, readonly=True)
    type_id = fields.Many2one(
        'bss.state.import_type', "Type", required=True,
        domain=("['|', ('country_id', '=', country_id), "
                "('country_id', '=', False)]"))
    file = fields.Binary("File")
    given_file = fields.Boolean("Given File", readonly=True)
    comment = fields.Text("Comment", readonly=True)

    @api.multi
    @api.onchange('type_id')
    def _onchange_type_id(self):
        if not self.type_id:
            self.comment = ""
            self.given_file = False
        self.comment = self.type_id.comment
        self.given_file = self.type_id.given_file
        self.file = False

    @api.multi
    def execute(self):
        assert len(self) == 1, "Can only be called on one record"
        wiz = self[0]
        getattr(self, wiz.type_id.method)()
        return True

    @api.multi
    def import_default(self):
        assert len(self) == 1, "Can only be called on one record"
        wiz = self[0]

        Lang = self.env['res.lang']
        State = self.env['res.country.state']

        file_input = io.BytesIO(base64.b64decode(wiz.file))
        self._logger.info(file_input)
        csv_lines = file_input.read().replace(
            '\xef\xbb\xbf', '').strip().split('\n')
        self._logger.info(csv_lines)
        reader = csv.reader(csv_lines, delimiter=',')

        headers = reader.next()
        if len(headers) < 2:
            raise osv.except_osv(
                _("Error!"),
                _("Incorrect file format, at least 2 columns expected.")
            )
        if headers[0] != 'code':
            raise osv.except_osv(
                _("Error!"),
                _("First column header must be 'code'.")
            )
        if headers[1] != 'name':
            raise osv.except_osv(
                _("Error!"),
                _("Second column header must be 'name'.")
            )
        lang_columns = {}
        for i in range(len(headers[2:])):
            header = headers[i + 2]
            terms = header.split(':')
            if len(terms) != 2 or terms[0] != 'name':
                raise osv.except_osv(
                    _("Error!"),
                    _("Incorrect column format. Column '%s' must be in format "
                      "'name:[LOCALE_CODE]'") % header
                )
            iso_code = terms[1]
            langs = Lang.search([('iso_code', '=', iso_code)])
            if not langs:
                self._logger.warn("'%s' is not an installed language code, "
                                  "column will be ignored" % iso_code)
                continue
            lang_columns[i + 2] = iso_code

        state_ids = []
        rows = list(reader)
        total = len(rows)
        count = 1
        for row in rows:
            self._logger.info("Import '%s' [%d/%d]", row[1], count, total)
            existing_states = State.search([
                ('country_id', '=', wiz.country_id.id),
                ('code', '=', row[0])])
            state = None
            if existing_states:
                state = existing_states[0]

            existing_inactive_states = State.search([
                ('country_id', '=', wiz.country_id.id),
                ('code', '=', row[0]),
                ('active', '=', False)])
            if existing_inactive_states:
                existing_inactive_states[0].active = True

            if state:
                state.name = row[1]
            else:
                state = State.create({
                    'code': row[0],
                    'name': row[1],
                    'country_id': wiz.country_id.id,
                })
            for column, iso_code in lang_columns.items():
                state.with_context(lang=iso_code).name = row[column]
            state_ids.append(state.id)
            count += 1

        State.search([
            ('country_id', '=', wiz.country_id.id),
            ('id', 'not in', state_ids)]).write({'active': False})
        return True

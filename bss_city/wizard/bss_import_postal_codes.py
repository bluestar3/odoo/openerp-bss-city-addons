# -*- coding: utf-8 -*-
# Part of Postal Codes.
# See LICENSE file for full copyright and licensing details.

import base64
import csv
import io
from odoo import api, fields, models
from odoo.netsvc import logging
from odoo.osv import osv
from odoo.tools.translate import _


class BssImportPostalCodes(models.TransientModel):
    _name = 'bss.import_postal_codes'
    _description = 'Import Postal Codes'
    _logger = logging.getLogger(_name)

    country_id = fields.Many2one(
        'res.country', "Country", required=True, readonly=True)
    type_id = fields.Many2one(
        'bss.postal_code.import_type', "Type", required=True,
        domain=("['|', ('country_id', '=', country_id), "
                "('country_id', '=', False)]"))
    file = fields.Binary("File")
    given_file = fields.Boolean("Given File", readonly=True)
    comment = fields.Text("Comment", readonly=True)

    @api.multi
    @api.onchange('type_id')
    def _onchange_type_id(self):
        if not self.type_id:
            self.comment = ""
            self.given_file = False
        self.comment = self.type_id.comment
        self.given_file = self.type_id.given_file
        self.file = False

    @api.multi
    def execute(self):
        self.ensure_one()
        wiz = self[0]
        getattr(self, wiz.type_id.method)()
        return True

    @api.multi
    def import_default(self):
        self.ensure_one()
        wiz = self[0]

        State = self.env['res.country.state']
        PostalCode = self.env['bss.postal_code']

        file_input = io.BytesIO(base64.b64decode(wiz.file))
        csv_lines = file_input.read().replace(
            '\xef\xbb\xbf', '').strip().split('\n')
        reader = csv.reader(csv_lines, delimiter=';')

        headers = reader.next()
        if len(headers) != 4:
            raise osv.except_osv(
                _("Error!"),
                _("Incorrect file format, 5 columns expected.")
            )
        if headers[0] != 'zip':
            raise osv.except_osv(
                _("Error!"),
                _("First column header must be 'zip'.")
            )
        if headers[1] != 'short_name':
            raise osv.except_osv(
                _("Error!"),
                _("Second column header must be 'short_name'.")
            )
        if headers[2] != 'long_name':
            raise osv.except_osv(
                _("Error!"),
                _("Third column header must be 'long_name'.")
            )
        if headers[3] != 'state_code':
            raise osv.except_osv(
                _("Error!"),
                _("Fifth column header must be 'state_code'.")
            )

        PostalCode.search([('country_id', '=', wiz.country_id.id)]).unlink()

        rows = list(reader)
        total = len(rows)
        count = 1
        for row in rows:
            self._logger.info("Import '%s %s' [%d/%d]",
                              row[0], row[2], count, total)
            states = State.search([
                ('code', '=', row[3]),
                ('country_id', '=', wiz.country_id.id)])
            PostalCode.create({
                'zip': row[0],
                'short_name': row[1],
                'long_name': row[2],
                'country_id': wiz.country_id.id,
                'state_id': states and states[0].id
            })
            count += 1
        return True

    def decoded_csv_reader(self, data, charset, **kwargs):
        csv_reader = csv.reader(data, **kwargs)
        for row in csv_reader:
            yield [cell.decode(charset).encode('utf-8') for cell in row]

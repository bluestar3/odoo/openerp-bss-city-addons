# -*- coding: utf-8 -*-
# Part of Postal Codes.
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields


class BssStateImportType(models.Model):
    _name = "bss.state.import_type"
    _description = "State Import Type"

    name = fields.Char("Name", translated=True)
    country_id = fields.Many2one('res.country', "Country")
    method = fields.Char("Method")
    given_file = fields.Boolean("Given File")
    comment = fields.Text("Comment")

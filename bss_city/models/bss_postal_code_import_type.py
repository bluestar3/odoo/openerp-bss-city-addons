# -*- coding: utf-8 -*-
# Part of Postal Codes.
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields


class BssPostalCodeImportType(models.Model):
    _name = "bss.postal_code.import_type"
    _description = "Postal Code Import Type"

    name = fields.Char("Name", translated=True)
    country_id = fields.Many2one('res.country', "Country")
    method = fields.Char("Method")
    given_file = fields.Boolean("Given File")
    comment = fields.Text("Comment")

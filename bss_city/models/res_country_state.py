# -*- coding: utf-8 -*-
# Part of Postal Codes.
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api


class ResCountryState(models.Model):
    _inherit = 'res.country.state'
    _sql_constraints = [
        ('unique_code_unique', 'unique(unique_code)',
         'Unique code has to be unique!')
    ]

    name = fields.Char(translate=True)
    active = fields.Boolean("Active", default=True)
    unique_code = fields.Char(
        "Unique Code", compute='_compute_unique_code', store=True)

    @api.multi
    @api.depends('country_id.code', 'code')
    def _compute_unique_code(self):
        for record in self:
            record.unique_code = '%(cc)s_%(sc)s' % {
                'cc': record.country_id.code,
                'sc': record.code
            }

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search(
                [('unique_code', 'ilike', name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()

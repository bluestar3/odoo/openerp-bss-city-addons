# -*- coding: utf-8 -*-
# Part of Postal Codes.
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class BssPostalCode(models.Model):
    _name = "bss.postal_code"
    _description = "Postal Code"
    _order = 'country_name, name asc'

    postal_code = fields.Char("Postal Code", size=10)
    short_name = fields.Char("Short Name", size=18)
    long_name = fields.Char("Long Name", size=27)
    state_id = fields.Many2one(
        'res.country.state', "State",
        domain="[('country_id', '=', country_id)]")
    country_id = fields.Many2one('res.country', "Country")
    country_name = fields.Char("Country Name", related='country_id.name')
    name = fields.Char("Name", compute='_compute_name', store=True)

    @api.multi
    @api.depends('postal_code', 'long_name')
    def _compute_name(self):
        for postal_code in self:
            postal_code.name = '%(zip)s %(city)s' % {
                'zip': postal_code.postal_code,
                'city': postal_code.long_name
            }

    @api.multi
    @api.constrains('state_id', 'country_id')
    def _check_country_id(self):
        for record in self:
            if (record.state_id and
                    record.country_id.id != record.state_id.country_id.id):
                raise ValidationError(
                    "Country of state must be the same of country "
                    "of postal code")

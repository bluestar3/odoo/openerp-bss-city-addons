# -*- coding: utf-8 -*-
# Part of Postal Codes.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Postal Codes',
    'version': '10.0.1.0.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Postal Codes
============

This is a technical addon to define common structure to store cities
(used by localized cities addons).
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://www.blues2.ch',
    'depends': ['sales_team'],
    'data': [
        'security/ir.model.access.csv',

        'data/bss_postal_code_import_type_data.xml',
        'data/bss_state_import_type_data.xml',

        'views/res_country_views.xml',
        'views/res_country_state_views.xml',
        'views/bss_postal_code_views.xml',

        'wizard/bss_import_postal_codes_views.xml',
        'wizard/bss_import_states_views.xml',
    ],
    'demo': [
        'data/res_country_state_demo.yml',
        'data/bluestar_postal_code_demo.yml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [],
}

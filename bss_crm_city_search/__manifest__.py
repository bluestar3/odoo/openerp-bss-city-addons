# -*- coding: utf-8 -*-
# Part of CRM Leads Postal Codes Search.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'CRM Leads Postal Codes Search',
    'version': '10.0.1.0.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
CRM Leads Postal Codes Search
=============================

Add a search field in lead form view. The search field can search data by
postal codes or city name.

When a user selects a city, the module fills the following fields
automatically  : postal code, city, state and country.

It depends on an external module to import the data, for example bss_city_ch.
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://www.blues2.ch',
    'depends': ["crm", "bss_city"],
    'data': ['views/crm_lead_views.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [
        'images/city_search_1.png',
        'images/city_search_2.png',
        'images/city_search_3.png',
    ],
}

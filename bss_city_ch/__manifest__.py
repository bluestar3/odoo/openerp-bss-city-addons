# -*- coding: utf-8 -*-
# Part of Switzerland Postal Codes.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Switzerland Postal Codes',
    'version': '10.0.1.0.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Switzerland Postal Codes
========================

This module imports a list of all swiss postal codes and cities.
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://www.blues2.ch',
    'depends': ['bss_city'],
    'data': [
        'data/bss_state_import_type_data.xml',
        'data/bss_postal_code_import_type_data.xml',
    ],
    'demo': [
        'data/res_country_state_demo.yml',
        'data/bss_postal_code_demo.yml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [],
}

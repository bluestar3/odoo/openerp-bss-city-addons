# -*- coding: utf-8 -*-
# Part of French Postal Codes.
# See LICENSE file for full copyright and licensing details.


import base64
import csv
import io
from odoo import models, api
from odoo.netsvc import logging
from odoo.osv import osv
from odoo.tools.translate import _


class BssImportPostalCodes(models.TransientModel):
    _inherit = 'bss.import_postal_codes'
    _logger = logging.getLogger(_inherit)

    @api.multi
    def import_data_gouv_fr(self):
        self.ensure_one()
        wiz = self[0]

        PostalCode = self.env['bss.postal_code']

        file_input = io.BytesIO(base64.b64decode(wiz.file))
        csv_lines = file_input.read().replace(
            '\xef\xbb\xbf', '').strip().split('\n')
        reader = csv.reader(csv_lines, delimiter=';')

        headers = reader.next()
        postal_code_index = -1
        label_index = -1
        line5_index = -1
        for i in range(len(headers)):
            if headers[i] == "Code_postal":
                postal_code_index = i
            if headers[i] == "Libelle_acheminement":
                label_index = i
            if headers[i] == 'Ligne_5':
                line5_index = i
        if postal_code_index == -1:
            raise osv.except_osv(
                _("Error!"),
                _("No column 'Code_postal'.")
            )
        if label_index == -1:
            raise osv.except_osv(
                _("Error!"),
                _("No column 'Libelle_acheminement'.")
            )
        if line5_index == -1:
            raise osv.except_osv(
                _("Error!"),
                _("No column 'Ligne_5'.")
            )

        PostalCode.search(
            [('country_id', '=', wiz.country_id.id)]).unlink()

        rows = list(reader)
        total = len(rows)
        count = 1
        for row in rows:
            name = row[line5_index] or row[label_index]
            self._logger.info(
                "Import '%s %s' [%d/%d]",
                row[postal_code_index], name, count, total)
            PostalCode.create({
                'postal_code': row[postal_code_index],
                'short_name': name,
                'long_name': name,
                'country_id': wiz.country_id.id,
                'state_id': None,
            })
            count += 1
        return True

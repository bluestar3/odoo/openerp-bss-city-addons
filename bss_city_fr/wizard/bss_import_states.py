# -*- coding: utf-8 -*-
# Part of French Postal Codes.
# See LICENSE file for full copyright and licensing details.

import base64
from odoo import api, models
from odoo.netsvc import logging
from odoo.modules.module import get_module_resource


class BssImportStates(models.TransientModel):
    _inherit = 'bss.import_states'
    _logger = logging.getLogger(_inherit)

    @api.multi
    def import_default_given_fr(self):
        self.ensure_one()
        filename = get_module_resource(
            'bss_city_fr', 'static/src/data', 'state_fr_201512.csv')
        self[0].file = base64.b64encode(open(filename).read())
        self.import_default()
